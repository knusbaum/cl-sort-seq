(asdf:defsystem :cl-sort-seq
  :name "cl-sort-seq"
  :description "Sorted sequences for extensible user sequences"
  :license "BSD"
  :author "Kyle Nusbaum"
  :components ((:file "package")
               (:file "common"
                      :depends-on ("package"))
               (:file "cl-sort-seq-lazy"
                      :depends-on ("common"))
               (:file "cl-sort-seq"
                      :depends-on ("common"))))

