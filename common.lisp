(in-package :cl-sort-seq)

(defgeneric re-sort (o)
  (:documentation
   #.(format nil "Calling this function on a sorted seq will cause the~@
                  sequence to be re-sorted. This should never be necessary~@
                  unless you have manually messed with the object internals.")))

