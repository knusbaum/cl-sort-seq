(in-package :cl-sort-seq)

(declaim (optimize (speed 3) (safety 0) (debug 3) (compilation-speed 0)))

(defclass sort-seq (standard-object sequence)
  ((arr :initarg :seq :initform (make-array 0))
   (comparator :initarg :comparator :type function)
   (sorted :initform nil)))

(defun make-sort-seq (seq comparator)
  (make-instance 'sort-seq :seq seq :comparator comparator))

(defmethod initialize-instance :after ((ss sort-seq) &key seq)
  (when (not (subtypep (type-of seq) 'simple-array))
    (setf (slot-value ss 'arr) (make-array (length seq) :initial-contents seq)))
  ss)

(defmethod re-sort ((ss sort-seq))
  (with-slots (arr comparator sorted) ss
    (sort arr comparator)
    (setf sorted t)))

(defmethod print-object ((o sort-seq) stream)
  (print-unreadable-object (o stream :type t)
    (with-slots (arr) o
      (declare (type simple-array arr)
               (type stream stream))
      (if (< (length arr) 100)
          (loop for i below (length o)
                do (if (= i 0)
                       (format stream "~a" (elt o i))
                       (format stream " ~a" (elt o i))))
          (format stream "~d elements" (length arr))))))

(defmethod sequence:length ((ss sort-seq))
  (with-slots (arr) ss
    (declare (type simple-array arr))
    (length arr)))

(defmethod sequence:elt ((ss sort-seq) index)
  (with-slots (arr sorted) ss
    (when (not sorted)
      (re-sort ss))
    (aref arr index)))

(defmethod (setf sequence:elt) (value (ss sort-seq) index)
  (with-slots (arr sorted) ss
    (setf (aref arr index) value)
    (setf sorted nil))
  value)

(defmethod sequence:adjust-sequence ((ss sort-seq) length &key initial-element initial-contents)
  (with-slots (arr sorted) ss
    (setf arr
          (sequence:adjust-sequence arr length
                                    :initial-element initial-element
                                    :initial-contents initial-contents))
    (setf sorted nil))
  ss)

(defmethod sequence:make-sequence-like ((ss sort-seq) length &key initial-element initial-contents)
  (when (and initial-element initial-contents)
      (error "supplied both :INITIAL-ELEMENT and :INITIAL-CONTENTS to CL-SORT-SEQ::MAKE-SEQUENCE-LIKE"))
  (with-slots (arr sort-start heap-ok comparator) ss
    (let* ((new-arr (cond
                      (initial-contents
                       (sequence:make-sequence-like arr length
                                                    :initial-contents initial-contents))
                      (initial-element
                       (sequence:make-sequence-like arr length
                                                    :initial-element initial-element))
                      (t (sequence:make-sequence-like arr length))))


           (new-seq (make-instance 'sort-seq
                                   :arr new-arr
                                   :comparator comparator)))
      (when (or (not (= length (length arr))) initial-contents)
        ;; This sequence has changed contents, resort.
        (setf (slot-value new-seq 'sorted) nil))
      new-seq)))

