(in-package :cl-sort-seq)

(declaim (optimize (speed 3) (safety 0) (debug 3) (compilation-speed 0)))

(defclass lazy-sort-seq (standard-object sequence)
  ((arr :initarg :seq :initform (make-array 0))
   (comparator :initarg :comparator :initform #'< :type function)
   (sort-start :initform 0 :type fixnum)
   (heap-ok :initform nil)))

(defun make-lazy-sort-seq (seq comparator)
  (make-instance 'lazy-sort-seq :seq seq :comparator comparator))

(defmethod initialize-instance :after ((lss lazy-sort-seq) &key seq)
  (when (not (subtypep (type-of seq) 'simple-array))
    (setf (slot-value lss 'arr) (make-array (length seq) :initial-contents seq)))
  lss)

(defmethod re-sort ((lss lazy-sort-seq))
  (with-slots (heap-ok sort-start) lss
    (setf heap-ok nil)
    (setf sort-start 0)))

(defmethod print-object ((o lazy-sort-seq) stream)
  (print-unreadable-object (o stream :type t)
    (with-slots (arr) o
      (declare (type simple-array arr)
               (type stream stream))
      (if (< (length arr) 100)
          (loop for i below (length o)
                do (if (= i 0)
                       (format stream "~a" (elt o i))
                       (format stream " ~a" (elt o i))))
          (format stream "~d elements" (length arr))))))

(defmacro parent-index (arr i)
  `(let ((eval-i ,i))
     (declare (type simple-array ,arr)
              (type fixnum eval-i))
     (truncate (+ eval-i (length ,arr)) 2)))

(defmacro left-child (arr i)
  `(let ((eval-i ,i))
     (declare (type simple-array ,arr)
              (type fixnum eval-i))
     (let ((times-two (* 2 eval-i)))
       (declare (type fixnum times-two))
       (- times-two (length ,arr) 1))))

(defmacro right-child (arr i)
  `(let ((eval-i ,i))
     (declare (type simple-array ,arr)
              (type fixnum eval-i))
     (let ((times-two (* 2 eval-i)))
       (declare (type fixnum times-two))
       (- times-two (length ,arr)))))

;;; Fast versions
(defun heapify (lss root)
  (with-slots (arr) lss
    (declare (type simple-array arr))
    (loop for start from (parent-index arr (1- (length arr))) downto 0
          do (sift-down lss start))))

(defun sift-down (lss root)
  (with-slots (arr sort-start comparator) lss
    (declare (type fixnum root sort-start)
             (type simple-array arr)
             (type function comparator))
    (loop with swapi fixnum = root
          for right-idx fixnum = (right-child arr root)
          while (>= right-idx sort-start)
          do (progn
               (when (funcall comparator (aref arr right-idx) (aref arr swapi))
                 (setf swapi right-idx))
               (when (and
                      (>= (1- right-idx) sort-start)
                      (funcall comparator (aref arr (1- right-idx)) (aref arr swapi)))
                 (setf swapi (1- right-idx)))
               (when (= swapi root)
                 (return-from sift-down))
               (rotatef (aref arr root) (aref arr swapi))
               (setf root swapi)))))

(defun full-sort (lss)
  (with-slots (arr sort-start heap-ok) lss
    (declare (type fixnum sort-start)
             (type simple-array arr))
    (heapify lss (1- (length arr)))
    (setf heap-ok t)

    (setf sort-start 0)
    (loop for i below (length arr)
       do (rotatef (elt arr sort-start) (elt arr (1- (length arr))))
         (incf sort-start)
         (sift-down lss (1- (length arr))))))

(defmethod sequence:length ((lss lazy-sort-seq))
  (with-slots (arr) lss
    (declare (type simple-array arr))
    (length arr)))

(defmethod sequence:elt ((lss lazy-sort-seq) index)
  (with-slots (arr sort-start heap-ok) lss
    (declare (type fixnum sort-start index)
             (type simple-array arr))
    (when (not heap-ok)
      (heapify lss (1- (length arr)))
      (setf heap-ok t))

    (when (< index (length arr))
      (loop while (<= sort-start index)
         do (rotatef (aref arr sort-start) (aref arr (1- (length arr))))
           (incf sort-start)
           (sift-down lss (1- (length arr)))))
    (aref arr index)))

(defmethod (setf sequence:elt) (value (lss lazy-sort-seq) index)
  (with-slots (arr sort-start heap-ok) lss
    (setf heap-ok nil)
    (setf sort-start 0)
    (setf (elt arr index) value)))

(defmethod sequence:adjust-sequence ((lss lazy-sort-seq) length &key initial-element initial-contents)
  (with-slots (arr sort-start heap-ok) lss
    (setf arr
          (sequence:adjust-sequence arr length
                                    :initial-element initial-element
                                    :initial-contents initial-contents))
    (setf sort-start 0)
    (setf heap-ok nil))
  lss)

(defmethod sequence:make-sequence-like ((lss lazy-sort-seq) length &key initial-element initial-contents)
  (when (and initial-element initial-contents)
      (error "supplied both :INITIAL-ELEMENT and :INITIAL-CONTENTS to CL-SORT-SEQ::MAKE-SEQUENCE-LIKE"))
  (with-slots (arr sort-start heap-ok comparator) lss
    (let* ((new-arr (cond
                      (initial-contents
                       (sequence:make-sequence-like arr length
                                                    :initial-contents initial-contents))
                      (initial-element
                       (sequence:make-sequence-like arr length
                                                    :initial-element initial-element))
                      (t (sequence:make-sequence-like arr length))))


           (new-seq (make-instance 'lazy-sort-seq
                                   :arr new-arr
                                   :comparator comparator)))
      (when (and (= length (length arr)) (not initial-element) (not initial-contents))
        ;; This sequence has not changed contents, keep the sorting.
        (setf (slot-value new-seq 'sort-start) sort-start)
        (setf (slot-value new-seq 'heap-ok) heap-ok))
      new-seq)))

