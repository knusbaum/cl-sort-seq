

(defun test-sort-seq ()
  (let* ((size 10000000)
         (rlist (make-rand-list size)))
    (format t "Getting 1st element.~%")
    (time
     ;; Get 1st element
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (elt seq 0)))

    (format t "Getting 1000th element.~%")
    (time
     ;; Get 1000th element
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (elt seq 1000)))
    
    (format t "Getting last element.~%")
    (time
     ;; Get 1000th element
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (elt seq (1- size))))
    
    (format t "Getting first 1000 elements.~%")
    (time
     ;; Get first 1000 elements
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (loop for i below 1000
          collecting (elt seq i))))

    (format t "Testing altering element at index 1000.~%")
    (time
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (setf (elt seq 1000) (random size))))

    (format t "Testing altering 100 elements beginning at index 1000.~%")
    (time
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (loop for i from 1000 below 1100
          do (setf (elt seq i) (random size)))))

    (format t "Testing altering element at index 1000, then getting the element.~%")
    (time
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (setf (elt seq 1000) (random size))
       (elt seq 1000)))

    (format t "Testing altering 100 elements beginning at index 1000, then getting them.~%")
    (time
     (let ((seq (cl-sort-seq:make-sort-seq rlist #'<)))
       (loop for i from 1000 below 1100
          do (setf (elt seq i) (random size)))
       (loop for i from 1000 below 1100
          collect (elt seq i)))))
  nil)

(defun test-lazy-sort-seq ()
  (let* ((size 10000000)
         (rlist (make-rand-list size)))
    (format t "Getting 1st element.~%")
    (time
     ;; Get 1st element
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (elt seq 0)))

    (format t "Getting 1000th element.~%")
    (time
     ;; Get 1000th element
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (elt seq 1000)))

    (format t "Getting last element.~%")
    (time
     ;; Get 1000th element
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (elt seq (1- size))))

    (format t "Getting first 1000 elements.~%")
    (time
     ;; Get first 1000 elements
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (loop for i below 1000
          collecting (elt seq i))))

    (format t "Testing altering element at index 1000.~%")
    (time
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (setf (elt seq 1000) (random size))))

    (format t "Testing altering 100 elements beginning at index 1000.~%")
    (time
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (loop for i from 1000 below 1100
          do (setf (elt seq i) (random size)))))

    (format t "Testing altering element at index 1000, then getting the element.~%")
    (time
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (setf (elt seq 1000) (random size))
       (elt seq 1000)))

    (format t "Testing altering 100 elements beginning at index 1000, then getting them.~%")
    (time
     (let ((seq (cl-sort-seq:make-lazy-sort-seq rlist #'<)))
       (loop for i from 1000 below 1100
          do (setf (elt seq i) (random size)))
       (loop for i from 1000 below 1100
          collect (elt seq i)))))
  nil)
