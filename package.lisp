(defpackage :cl-sort-seq
  (:use :cl)
  (:export

   ;; Sorted Sequence Classes
   :lazy-sort-seq
   :sort-seq

   ;; Constructors
   :make-lazy-sort-seq
   :make-sort-seq
   
   ;; Helper Functions
   :re-sort))
